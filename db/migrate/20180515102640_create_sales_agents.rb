class CreateSalesAgents < ActiveRecord::Migration
  def change
    create_table :sales_agents do |t|
      t.string :surname
      t.string :other_names
      t.date :dob
      t.string :gender
      t.string :tel_number
      t.string :location_address
      t.string :age
      t.boolean :active_status
      t.boolean :del_status
      t.integer :user_id

      t.timestamps null: false
    end
  end
end
