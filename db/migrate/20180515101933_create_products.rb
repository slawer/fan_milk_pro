class CreateProducts < ActiveRecord::Migration
  def change
    create_table :products do |t|
      t.integer :category_id
      t.string :product_name
      t.integer :user_id
      t.boolean :active_status
      t.boolean :del_status
      t.integer :price_id

      t.timestamps null: false
    end
  end
end
