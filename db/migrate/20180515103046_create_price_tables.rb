class CreatePriceTables < ActiveRecord::Migration
  def change
    create_table :price_tables do |t|
      t.decimal :price,precision: 8, scale: 2
      t.boolean :active_status
      t.boolean :del_status
      t.integer :user_id

      t.timestamps null: false
    end
  end
end
