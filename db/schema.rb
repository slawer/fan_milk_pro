# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20180515103046) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "price_tables", force: :cascade do |t|
    t.decimal  "price",         precision: 8, scale: 2
    t.boolean  "active_status"
    t.boolean  "del_status"
    t.integer  "user_id"
    t.datetime "created_at",                            null: false
    t.datetime "updated_at",                            null: false
  end

  create_table "product_categories", force: :cascade do |t|
    t.string   "category_name"
    t.integer  "user_id"
    t.boolean  "active_status"
    t.boolean  "del_status"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  create_table "products", force: :cascade do |t|
    t.integer  "category_id"
    t.string   "product_name"
    t.integer  "user_id"
    t.boolean  "active_status"
    t.boolean  "del_status"
    t.integer  "price_id"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  create_table "sales", force: :cascade do |t|
    t.integer  "product_id"
    t.integer  "quantity"
    t.integer  "agent_id"
    t.datetime "date_time"
    t.integer  "user_id"
    t.boolean  "active_status"
    t.boolean  "del_status"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  create_table "sales_agents", force: :cascade do |t|
    t.string   "surname"
    t.string   "other_names"
    t.date     "dob"
    t.string   "gender"
    t.string   "tel_number"
    t.string   "location_address"
    t.string   "age"
    t.boolean  "active_status"
    t.boolean  "del_status"
    t.integer  "user_id"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
  end

end
