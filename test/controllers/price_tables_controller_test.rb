require 'test_helper'

class PriceTablesControllerTest < ActionController::TestCase
  setup do
    @price_table = price_tables(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:price_tables)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create price_table" do
    assert_difference('PriceTable.count') do
      post :create, price_table: { active_status: @price_table.active_status, del_status: @price_table.del_status, price: @price_table.price, user_id: @price_table.user_id }
    end

    assert_redirected_to price_table_path(assigns(:price_table))
  end

  test "should show price_table" do
    get :show, id: @price_table
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @price_table
    assert_response :success
  end

  test "should update price_table" do
    patch :update, id: @price_table, price_table: { active_status: @price_table.active_status, del_status: @price_table.del_status, price: @price_table.price, user_id: @price_table.user_id }
    assert_redirected_to price_table_path(assigns(:price_table))
  end

  test "should destroy price_table" do
    assert_difference('PriceTable.count', -1) do
      delete :destroy, id: @price_table
    end

    assert_redirected_to price_tables_path
  end
end
