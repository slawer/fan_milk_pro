json.extract! sales_agent, :id, :surname, :other_names, :dob, :gender, :tel_number, :location_address, :age, :active_status, :del_status, :user_id, :created_at, :updated_at
json.url sales_agent_url(sales_agent, format: :json)
