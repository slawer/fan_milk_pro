json.extract! price_table, :id, :price, :active_status, :del_status, :user_id, :created_at, :updated_at
json.url price_table_url(price_table, format: :json)
