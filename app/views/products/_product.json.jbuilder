json.extract! product, :id, :category_id, :product_name, :user_id, :active_status, :del_status, :price_id, :created_at, :updated_at
json.url product_url(product, format: :json)
