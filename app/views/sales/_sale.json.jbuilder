json.extract! sale, :id, :product_id, :quantity, :agent_id, :date_time, :user_id, :active_status, :del_status, :created_at, :updated_at
json.url sale_url(sale, format: :json)
