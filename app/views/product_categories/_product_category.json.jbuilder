json.extract! product_category, :id, :category_name, :user_id, :active_status, :del_status, :created_at, :updated_at
json.url product_category_url(product_category, format: :json)
