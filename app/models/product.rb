class Product < ActiveRecord::Base

  belongs_to :product_category, class_name: 'ProductCategory', foreign_key: :category_id
  belongs_to :price_table, class_name: 'PriceTable', foreign_key: :price_id
end
