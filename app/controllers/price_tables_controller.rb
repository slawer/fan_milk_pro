class PriceTablesController < ApplicationController
  before_action :set_price_table, only: [:show, :edit, :update, :destroy]

  # GET /price_tables
  # GET /price_tables.json
  def index
    @price_tables = PriceTable.all
  end

  # GET /price_tables/1
  # GET /price_tables/1.json
  def show
  end

  # GET /price_tables/new
  def new
    @price_table = PriceTable.new
  end

  # GET /price_tables/1/edit
  def edit
  end

  # POST /price_tables
  # POST /price_tables.json
  def create
    @price_table = PriceTable.new(price_table_params)

    respond_to do |format|
      if @price_table.save
        format.html { redirect_to @price_table, notice: 'Price table was successfully created.' }
        format.json { render :show, status: :created, location: @price_table }
      else
        format.html { render :new }
        format.json { render json: @price_table.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /price_tables/1
  # PATCH/PUT /price_tables/1.json
  def update
    respond_to do |format|
      if @price_table.update(price_table_params)
        format.html { redirect_to @price_table, notice: 'Price table was successfully updated.' }
        format.json { render :show, status: :ok, location: @price_table }
      else
        format.html { render :edit }
        format.json { render json: @price_table.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /price_tables/1
  # DELETE /price_tables/1.json
  def destroy
    @price_table.destroy
    respond_to do |format|
      format.html { redirect_to price_tables_url, notice: 'Price table was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_price_table
      @price_table = PriceTable.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def price_table_params
      params.require(:price_table).permit(:price, :active_status, :del_status, :user_id)
    end
end
